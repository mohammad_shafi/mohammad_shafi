# Hi 👋, I'm Syed Mohammad Shafi 

<h3 align="center">A meticulous engineer crafting precise digital solutions.</h3>

- 🏢  I am currently employed at  **Trigent Software Pvt. Ltd.**

- 📫 How to reach me **mohammad_s@trigent.com**

